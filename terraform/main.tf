terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.74"
}

provider "yandex" {
  token     = var.ya_token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = "ru-central1-b"
}

resource "yandex_iam_service_account" "vmadmin" {
  name        = "vmadmin"
  description = "service account to manage VMs"
}


resource "yandex_resourcemanager_folder_iam_member" "vm-editor-role" {
  folder_id = var.folder_id
  role      = "editor"
  member    = "serviceAccount:${yandex_iam_service_account.vmadmin.id}"
}

resource "yandex_compute_instance_group" "s-group1" {
  name               = "docker-vms"
  service_account_id = yandex_iam_service_account.vmadmin.id
  depends_on = [yandex_iam_service_account.vmadmin, yandex_resourcemanager_folder_iam_member.vm-editor-role]
  instance_template {
    name = "docker-{instance.index}"
    platform_id = "standard-v3"
    resources {
      cores         = 2
      memory        = 1
      core_fraction = 20
    }
    boot_disk {
      initialize_params {
        image_id = "fd82re2tpfl4chaupeuf"
        size     = 10
      }
    }
    network_interface {
      network_id = yandex_vpc_network.s-network-1.id
      subnet_ids = [yandex_vpc_subnet.s-subnet-1.id, yandex_vpc_subnet.s-subnet-2.id]
      nat = true
    }
    metadata = {
      ssh-keys = "ubuntu:${file(var.ssh_public_key)}"
    }

  }
  load_balancer {
    target_group_name = "balancer-target-group"
  }
  allocation_policy {
    zones = ["ru-central1-b", "ru-central1-a"]
  }
  deploy_policy {
    max_unavailable = 2
    max_creating    = 2
    max_expansion   = 2
    max_deleting    = 2
  }
  scale_policy {
    fixed_scale {
      size = var.size
    }
  }
}

resource "null_resource" "command" {
  count = var.size


  provisioner "remote-exec" {
    inline = [
      "ansible-venv",
      "ansible-playbook --key-file ~/.ssh/vds_rsa -i element(yandex_compute_instance_group.s-group1.instances[*].network_interface[0].nat_ip_address, count.index) ../ansible/server-preparation.yml"
    ]
  }
}

resource "yandex_vpc_network" "s-network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "s-subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.s-network-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

resource "yandex_vpc_subnet" "s-subnet-2" {
  name           = "subnet2"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.s-network-1.id
  v4_cidr_blocks = ["192.168.11.0/24"]
}

resource "yandex_lb_network_load_balancer" "s-balancer-1" {
  name = "react-network-load-balancer"

  listener {
    name = "react-listener"
    port = var.external_port
    target_port = var.service_port
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_compute_instance_group.s-group1.load_balancer.0.target_group_id
    healthcheck {
      name = "http"
      http_options {
        port = var.health_check_port
        path = "/health"
      }
    }
  }
}

resource "yandex_dns_zone" "zone1" {
  name        = "public-zone"
  description = "desc"

  labels = {
    label1 = "general"
  }

  zone             = "evaron.ru."
  public           = true
}

resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "sb.evaron.ru."
  type    = "A"
  ttl     = 200
  data    = yandex_lb_network_load_balancer.s-balancer-1.listener.*.external_address_spec[0].*.address
}

data "terraform_remote_state" "gitlab" {
  backend = "http"

  config = {
    address = var.remote_state_address
    username = var.gitlab_username
    password = var.gitlab_access_token
  }
}

output "docker-internal" {
  value = yandex_compute_instance_group.s-group1.instances.*.network_interface.0.ip_address
}

output "docker-external" {
  value = yandex_compute_instance_group.s-group1.instances.*.network_interface.0.nat_ip_address
}

output "balancer-external" {
  value = yandex_lb_network_load_balancer.s-balancer-1.listener.*.external_address_spec[0].*.address
}

variable "size" {
  type = string
}

variable "health_check_port" {
  type = number
}

variable "service_port" {
  type = number
}

variable "external_port" {
  type = number
}

variable "ya_token" {
  type = string
}

variable "cloud_id" {
  type = string
}

variable "folder_id" {
  type = string
}

variable "remote_state_address" {
  type = string
  description = "Gitlab remote state file address"
}

variable "gitlab_username" {
  type = string
  description = "Gitlab username to query remote state"
}

variable "gitlab_access_token" {
  type = string
  description = "GitLab access token to query remote state"
}

variable "ssh_public_key" {
  type = string
}
